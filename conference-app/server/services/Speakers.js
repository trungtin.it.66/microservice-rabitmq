// const fs = require('fs');
// const util = require('util');
// added
const axios = require('axios');
const url = require('url');
const crypto = require('crypto');

// cache image: start
const fs = require('fs');
const util = require('util');
const fsexists = util.promisify(fs.exists);
// cache image: end

const CircuitBreaker = require('../lib/CircuitBreaker');

const circuitBreaker = new CircuitBreaker();

// const readFile = util.promisify(fs.readFile);

class SpeakersService {

  // constructor(datafile) {
  //   this.datafile = datafile;
  // }

  constructor({ serviceRegistryUrl, serviceVersionIdentifier }) {
    this.serviceRegistryUrl = serviceRegistryUrl;
    this.serviceVersionIdentifier = serviceVersionIdentifier;
    // add caching
    this.cache = {};
  }

  // added
  async getImage(path) {
    const { ip, port } = await this.getService('speakers-service');
    return this.callService({
      method: 'get',
      responseType: 'stream',
      url: `http://${ip}:${port}/images/${path}`, // http://localhost:38281/images/speakers/Lorenzo_Garcia.jpg
    });
  }

  async getNames() {
    // const data = await this.getData();

    // return data.map(speaker => ({
    //   name: speaker.name,
    //   shortname: speaker.shortname,
    // }));

    const { ip, port } = await this.getService('speakers-service');
    return this.callService({
      method: 'get',
      url: `http://${ip}:${port}/names`,
    });
  }

  async getListShort() {
    // const data = await this.getData();
    // return data.map(speaker => ({
    //   name: speaker.name,
    //   shortname: speaker.shortname,
    //   title: speaker.title,
    // }));

    const { ip, port } = await this.getService('speakers-service');
    return this.callService({
      method: 'get',
      url: `http://${ip}:${port}/list-short`,
    });
  }

  async getList() {
    // const data = await this.getData();
    // return data.map(speaker => ({
    //   name: speaker.name,
    //   shortname: speaker.shortname,
    //   title: speaker.title,
    //   summary: speaker.summary,
    // }));

    const { ip, port } = await this.getService('speakers-service');
    return this.callService({
      method: 'get',
      url: `http://${ip}:${port}/list`,
    });
  }

  async getAllArtwork() {
    // const data = await this.getData();
    // const artwork = data.reduce((acc, elm) => {
    //   if (elm.artwork) {
    //     // eslint-disable-next-line no-param-reassign
    //     acc = [...acc, ...elm.artwork];
    //   }
    //   return acc;
    // }, []);
    // return artwork;

    const { ip, port } = await this.getService('speakers-service');
    return this.callService({
      method: 'get',
      url: `http://${ip}:${port}/artwork`,
    });
  }

  async getSpeaker(shortname) {
    // const data = await this.getData();
    // const speaker = data.find(current => current.shortname === shortname);
    // if (!speaker) return null;
    // return {
    //   title: speaker.title,
    //   name: speaker.name,
    //   shortname: speaker.shortname,
    //   description: speaker.description,
    // };

    const { ip, port } = await this.getService('speakers-service');
    return this.callService({
      method: 'get',
      url: `http://${ip}:${port}/speaker/${shortname}`,
    });
  }

  async getArtworkForSpeaker(shortname) {
    // const data = await this.getData();
    // const speaker = data.find(current => current.shortname === shortname);
    // if (!speaker || !speaker.artwork) return null;
    // return speaker.artwork;
    
    const { ip, port } = await this.getService('speakers-service');
    return this.callService({
      method: 'get',
      url: `http://${ip}:${port}/artwork/${shortname}`,
    });
  }

  // removed
  // async getData() {
  //   const data = await readFile(this.datafile, 'utf8');
  //   if (!data) return [];
  //   return JSON.parse(data).speakers;
  // }

  // added
  async callService(requestOptions) {
// ======normal: start
    // const response = await axios(requestOptions);
    // const result = response.data;
    // return result;
// ======normal: end

// ======circuitBreaker: start
    // return circuitBreaker.callService(requestOptions);
// ======circuitBreaker: end

// ======cache image: start
    const servicePath = url.parse(requestOptions.url).path;
    
    // generate cacheKey
    const cacheKey = crypto.createHash('md5').update(requestOptions.method + servicePath).digest('hex');
        
    
    let cacheFile = null;

    if (requestOptions.responseType && requestOptions.responseType === 'stream') {
      cacheFile = `${__dirname}/../../_imagecache/${cacheKey}`;
    }
  
    // circuitBreaker: start
    const result = await circuitBreaker.callService(requestOptions);
    // circuitBreaker: end

    if (!result) {
      if (this.cache[cacheKey]) return this.cache[cacheKey];
      if (cacheFile) {
        const exists = await fsexists(cacheFile);
        if (exists) return fs.createReadStream(cacheFile);
      }
      return false;
    }

    if (!cacheFile) {
      this.cache[cacheKey] = result;
    } else {
      const ws = fs.createWriteStream(cacheFile);
      result.pipe(ws);
    }
    return result;    
// ======cache image: end

    // const result = await circuitBreaker.callService(requestOptions);

    // if (!result) {
    //   if (this.cache[cacheKey]) return this.cache[cacheKey];
    //   return false;
    // }

    // this.cache[cacheKey] = result;
    // return result;
  }

  async getService(servicename) {
    // const response = await axios.get(`http://localhost:3000/find/${servicename}/1`);
    const response = await axios.get(`${this.serviceRegistryUrl}/find/${servicename}/${this.serviceVersionIdentifier}`);
    return response.data;
  }

}

module.exports = SpeakersService;
